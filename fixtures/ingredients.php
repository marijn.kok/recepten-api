<?php

declare(strict_types=1);

use App\Entity\Ingredient;

return [
    Ingredient::class => [
        'ingredient1'  => ['name' => 'Water'],
        'ingredient2'  => ['name' => 'Olijfolie'],
        'ingredient3'  => ['name' => 'Margarine'],
        'ingredient4'  => ['name' => 'Spaghetti'],
        'ingredient5'  => ['name' => 'Broccoli'],
        'ingredient6'  => ['name' => 'Spinazie'],
        'ingredient7'  => ['name' => 'Aubergine'],
        'ingredient8'  => ['name' => 'Ui'],
        'ingredient9'  => ['name' => 'Knoflook'],
        'ingredient10' => ['name' => 'Knoflook poeder'],
        'ingredient11' => ['name' => 'Boontjes'],
        'ingredient12' => ['name' => 'Bruine bonen'],
        'ingredient13' => ['name' => 'Haver'],
        'ingredient14' => ['name' => 'Typo 00 bloem'],
        'ingredient15' => ['name' => 'Volkoren bloem'],
        'ingredient16' => ['name' => 'Bloem'],
        'ingredient17' => ['name' => 'Semolina'],
        'ingredient18' => ['name' => 'Tomaten'],
        'ingredient19' => ['name' => 'Tomaten saus (fijn)'],
        'ingredient20' => ['name' => 'Tomaten saus (grof)'],
        'ingredient21' => ['name' => 'Basilicum (vers)'],
        'ingredient22' => ['name' => 'Basilicum'],
        'ingredient23' => ['name' => 'Aardbijen'],
        'ingredient24' => ['name' => 'Soja'],
        'ingredient25' => ['name' => 'Tofu'],
        'ingredient26' => ['name' => 'Andijvie'],
        'ingredient27' => ['name' => 'Kikkererwten'],
        'ingredient28' => ['name' => 'Champignons'],
        'ingredient29' => ['name' => 'Champignons'],
        'ingredient30' => ['name' => 'Wraps XL'],
        'ingredient31' => ['name' => 'Wraps'],
    ],
];
