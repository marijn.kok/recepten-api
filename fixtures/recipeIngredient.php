<?php

declare(strict_types=1);

use App\Entity\RecipeIngredient;
use App\Enum\Unit;

return [
    RecipeIngredient::class => [
        'recipeIngredient_{1..5}'   => [
            'recipe'      => '@recipe1',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{6..10}'  => [
            'recipe'      => '@recipe2',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{11..13}' => [
            'recipe'      => '@recipe3',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{14..25}' => [
            'recipe'      => '@recipe4',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{26..27}' => [
            'recipe'      => '@recipe5',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{28..32}' => [
            'recipe'      => '@recipe6',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{33..39}' => [
            'recipe'      => '@recipe7',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{40..45}' => [
            'recipe'      => '@recipe8',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{45..50}' => [
            'recipe'      => '@recipe9',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
        'recipeIngredient_{50..60}' => [
            'recipe'      => '@recipe10',
            'ingredient'  => '@ingredient<numberBetween(1, 31)>',
            'measurement' => '<numberBetween(7, 134)>',
            'unit'        => '<randomUnit()>',
        ],
    ],
];
