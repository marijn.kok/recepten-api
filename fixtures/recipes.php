<?php

declare(strict_types=1);

use App\Entity\Recipe;

return [
    Recipe::class => [
        'recipe1'  => [
            'name'        => 'Pizza',
            'createdAt'   => new DateTime('-1 year'),
            'updatedAt'   => new DateTime('-1 year +1 month'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe2'  => [
            'name'        => 'Burrito\'s',
            'createdAt'   => new DateTime('-6 months'),
            'updatedAt'   => new DateTime('-1 month'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe3'  => [
            'name'        => 'Taco\'s',
            'createdAt'   => new DateTime('-7 days'),
            'updatedAt'   => new DateTime('-6 days'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe4'  => [
            'name'        => 'Chickpea Curry',
            'createdAt'   => new DateTime('-7 days'),
            'updatedAt'   => new DateTime('-6 days'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe5'  => [
            'name'        => 'BQQ Jackfruit burger',
            'createdAt'   => new DateTime('-10 days'),
            'updatedAt'   => new DateTime('-10 days'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe6'  => [
            'name'        => 'Broccoli Pasta',
            'createdAt'   => new DateTime('-15 days'),
            'updatedAt'   => new DateTime('-14 days'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe7'  => [
            'name'        => 'Verse Pasta',
            'createdAt'   => new DateTime('-7 months'),
            'updatedAt'   => new DateTime('-6 days'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe8'  => [
            'name'        => 'Focaccia Pizza',
            'createdAt'   => new DateTime('-1 days'),
            'updatedAt'   => new DateTime('-1 days'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe9'  => [
            'name'        => 'Brood',
            'createdAt'   => new DateTime('-7 hours'),
            'updatedAt'   => new DateTime('-6 hours'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe10' => [
            'name'        => 'Stokbrood',
            'createdAt'   => new DateTime('-7 weeks'),
            'updatedAt'   => new DateTime('-6 weeks'),
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
        'recipe11' => [
            'name'        => 'Hummus',
            'description' => '<sentence()>',
            'author'      => '@user_*',
        ],
    ],
];
