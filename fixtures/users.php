<?php

declare(strict_types=1);

use App\Entity\User;

return [
    User::class => [
        'user_1'       => [
            'username' => 'user',
            'password' => 'test',
            'roles'    => [User::ROLE_USER],
        ],
        'admin_1'      => [
            'username' => 'admin',
            'password' => 'test',
            'roles'    => [User::ROLE_ADMIN],
        ],
        'user_{3..12}' => [
            'username' => '<username()>',
            'password' => 'test',
            'roles'    => [User::ROLE_ADMIN],
        ],
        'user_{5..15}' => [
            'username' => '<username()>',
            'password' => 'test',
            'roles'    => [User::ROLE_USER],
        ],
        'user_16'      => [
            'username' => '🙃',
            'password' => 'test',
            'roles'    => [User::ROLE_USER],
        ],
    ],
];
