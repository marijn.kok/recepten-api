<?php

declare(strict_types=1);

namespace App\EventSubscriber\Doctrine;

use App\Entity\AbstractEntity;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

class UpdatedAtSubscriber implements EventSubscriberInterface
{
    /**
     * @return array<string>
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::preUpdate,
        ];
    }

    public function preUpdate(LifecycleEventArgs $event): void
    {
        $object = $event->getObject();
        if (!$object instanceof AbstractEntity) {
            return;
        }

        $object->setUpdatedAt();
    }
}
