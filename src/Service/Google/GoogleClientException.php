<?php

declare(strict_types=1);

namespace App\Service\Google;

use Exception;

class GoogleClientException extends Exception
{
}
