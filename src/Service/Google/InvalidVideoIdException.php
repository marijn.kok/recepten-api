<?php

declare(strict_types=1);

namespace App\Service\Google;

use InvalidArgumentException;

class InvalidVideoIdException extends InvalidArgumentException
{
}
