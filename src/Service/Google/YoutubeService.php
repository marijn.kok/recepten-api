<?php

declare(strict_types=1);

namespace App\Service\Google;

use Google\Service\YouTube;
use Google\Service\YouTube\VideoListResponse;
use Google_Client;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class YoutubeService
{
    private ?YouTube $youTubeClient = null;

    public function __construct(
        private Google_Client $googleClient,
        #[Autowire('%env(GOOGLE_API_KEY)%')]
        private readonly ?string $googleApiKey = null
    ) {
    }

    public function hasValidCredentials(): void
    {
        if (empty($this->googleApiKey)) {
            throw new GoogleClientException('The google client is not enabled or has an invalid api key.');
        }
    }

    public function getVideo(string $youtube): ?YouTube\Video
    {
        $videoId = (new VideoResource($youtube))->getVideoId();
        /** @var VideoListResponse $response */
        $response = $this->getService()->videos->listVideos('snippet,contentDetails', ['id' => $videoId]);
        if ($response->count() <= 0) {
            return null;
        }

        $video = $response->current();
        if (!$video instanceof YouTube\Video) {
            throw new YoutubeVideoNotFoundException('No youtube video found with the ID: ' . $videoId);
        }

        return $video;
    }

    private function getService(): \Google_Service_YouTube
    {
        if (!$this->youTubeClient instanceof YouTube) {
            $this->youTubeClient = new YouTube($this->googleClient);
        }

        return $this->youTubeClient;
    }
}
