<?php

declare(strict_types=1);

namespace App\Service\Google;

class VideoResource
{
    public function __construct(private string $resource)
    {
    }

    public function getVideoId(): string
    {
        $videoId = $this->resource;
        if (filter_var($this->resource, FILTER_VALIDATE_URL)) {
            $videoId = $this->parseUrl();
        }

        if (mb_strlen($videoId) !== 11) {
            throw new InvalidVideoIdException('Invalid Youtube video ID given.');
        }

        return $videoId;
    }

    private function parseUrl(): string
    {
        if (!preg_match(
            '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i',
            $this->resource,
            $match
        )) {
            throw new InvalidVideoUrlException('Invalid Youtube URL.');
        }

        if (!is_string($match[1])) {
            throw new InvalidVideoUrlException('Could not fetch Youtube ID from URL');
        }

        return $match[1];
    }
}
