<?php

declare(strict_types=1);

namespace App\DataFixtures\Faker;

use App\Enum\Unit;
use Faker\Provider\Base as BaseProvider;

class UnitProvider extends BaseProvider
{
    public static function randomUnit(): string
    {
        return Unit::cases()[array_rand(Unit::cases())]->value;
    }
}
