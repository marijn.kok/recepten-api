<?php

declare(strict_types=1);

namespace App\Entity;

interface AuthorInterface
{
    public function getAuthor(): ?User;

    public function setAuthor(?User $author): AuthorInterface;
}
