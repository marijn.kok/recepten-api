<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\RecipeWebsiteDto;
use App\Dto\RecipeYoutubeDto;
use App\Repository\RecipeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\MaxDepth;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: RecipeRepository::class)]
#[ORM\Table()]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'post_youtube' => [
            'deprecation_reason' => 'Create a recipe website instead with the Youtube url.',
            'openapi_context'    => [
                'summary' => 'Create a Recipe from a youtube video',
            ],
            'method'             => 'post',
            'path'               => '/recipes/youtube',
            'input'              => RecipeYoutubeDto::class,
        ],
        'post_website' => [
            'openapi_context' => [
                'summary' => 'Create a Recipe from a website',
            ],
            'method'          => 'post',
            'path'            => '/recipes/website',
            'input'           => RecipeWebsiteDto::class,
        ],
    ],
    itemOperations: [
        'get' => [
            'security' => 'is_granted("ROLE_USER", object)',
        ],
        'put',
        'delete',
    ],
    subresourceOperations: [
        'ingredients_get_subresource' => [
            'method' => 'GET',
            'path'   => '/recipes/{id}/ingredients',
        ],
    ],
    denormalizationContext: ['groups' => ['write']],
    normalizationContext: ['groups' => ['recipe:read', 'read']],
)]
#[ApiFilter(BooleanFilter::class, properties: ['public'])]
#[ApiFilter(SearchFilter::class, properties: ['author' => 'exact'])]
class Recipe extends AbstractEntity implements AuthorInterface
{
    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['recipe:read', 'write'])]
    private string $name = '';

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['recipe:read', 'write'])]
    private ?string $description = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['recipe:read', 'write'])]
    #[Assert\Regex('/[a-z0-9]/i')]
    private ?string $youtube = null;

    #[ORM\Column(type: 'string', nullable: true)]
    #[Groups(['recipe:read', 'write'])]
    #[Assert\Url]
    private ?string $source = null;

    #[Orm\Column(type: 'boolean', nullable: false)]
    #[Groups(['recipe:read', 'write'])]
    private bool $public = true;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['recipe:read', 'read'])]
    private ?User $author = null;

    /**
     * @var Collection<int, RecipeIngredient>
     */
    #[ORM\OneToMany(mappedBy: 'recipe', targetEntity: RecipeIngredient::class, cascade: ['all'], orphanRemoval: true)]
    #[ApiSubresource]
    #[Groups(['recipe:read'])]
    #[MaxDepth(1)]
    private Collection $ingredients;

    public function __construct()
    {
        parent::__construct();
        $this->ingredients = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Recipe
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Recipe
    {
        $this->description = $description;

        return $this;
    }

    public function getYoutube(): ?string
    {
        return $this->youtube;
    }

    public function setYoutube(?string $youtube): Recipe
    {
        $this->youtube = $youtube;

        return $this;
    }

    public function getSource(): ?string
    {
        return $this->source;
    }

    public function setSource(?string $source): Recipe
    {
        $this->source = $source;

        return $this;
    }

    public function isPublic(): bool
    {
        return $this->public;
    }

    public function setPublic(bool $public): Recipe
    {
        $this->public = $public;

        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): Recipe
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection<int, RecipeIngredient>
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(RecipeIngredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients->add($ingredient);
            $ingredient->setRecipe($this);
        }

        return $this;
    }

    public function removeIngredient(RecipeIngredient $ingredient): self
    {
        if ($this->ingredients->removeElement($ingredient)) {
            // set the owning side to null (unless already changed)
            if ($ingredient->getRecipe() === $this) {
                $ingredient->setRecipe(null);
            }
        }

        return $this;
    }
}
