<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: 'App\Repository\IngredientRepository')]
#[ORM\Table()]
#[ORM\Index(name: "name_idx", columns: ["name"])]
#[ApiResource(
    collectionOperations: [
        'get'  => ['security' => 'is_granted("ROLE_USER", object)'],
        'post' => ['security' => 'is_granted("ROLE_USER", object)'],
    ],
    itemOperations: [
        'get',
        'put'    => ['security' => 'is_granted("ROLE_USER", object)'],
        'delete' => ['security' => 'is_granted("ROLE_USER", object)'],
    ],
    denormalizationContext: ['groups' => ['write']],
    normalizationContext: ['groups' => ['read']],
)]
class Ingredient extends AbstractEntity
{
    #[ORM\Column(type: 'string')]
    #[Assert\NotBlank()]
    #[Groups(['recipe:read'])]
    private string $name = '';

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['recipe:read'])]
    private ?string $description = null;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Ingredient
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Ingredient
    {
        $this->description = $description;

        return $this;
    }
}
