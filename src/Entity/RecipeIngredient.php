<?php

namespace App\Entity;

use ApiPlatform\Core\Action\NotFoundAction;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Enum\Unit;
use App\Repository\RecipeIngredientRepository;
use Doctrine\ORM\Mapping as ORM;
use Muffe\EnumConstraint\Constraints\Enum;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RecipeIngredientRepository::class)]
#[ApiResource(
    collectionOperations: [
        'post' => [
            'path'                    => '/recipes/ingredient',
            'denormalization_context' => ['groups' => ['write', 'update']],
            'security'                => 'is_granted("ROLE_USER", object)',
        ],
    ],
    itemOperations: [
        'get'    => [
            'controller' => NotFoundAction::class,
            'read'       => false,
            'output'     => false,
        ],
        'put'    => [
            'path'     => '/recipes/ingredient/{id}',
            'security' => 'is_granted("ROLE_USER", object)',
        ],
        'delete' => [
            'path'     => '/recipes/ingredient/{id}',
            'security' => 'is_granted("ROLE_USER", object)',
        ],
    ],
    denormalizationContext: ['groups' => ['update']],
)]
class RecipeIngredient
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'ingredients')]
    #[ORM\JoinColumn(nullable: false)]
    #[ApiProperty(readableLink: false, writableLink: false)]
    #[Groups(['write'])]
    private ?Recipe $recipe = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['write', 'update'])]
    private ?Ingredient $ingredient = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Groups(['write', 'update'])]
    private ?string $measurement = null;

    #[ORM\Column(length: 100, nullable: true)]
    #[Enum(enumType: Unit::class)]
    #[Groups(['write', 'update'])]
    private ?string $unit = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecipe(): ?Recipe
    {
        return $this->recipe;
    }

    public function setRecipe(?Recipe $recipe): self
    {
        $this->recipe = $recipe;

        return $this;
    }

    public function getIngredient(): ?Ingredient
    {
        return $this->ingredient;
    }

    public function setIngredient(?Ingredient $ingredient): self
    {
        $this->ingredient = $ingredient;

        return $this;
    }

    public function getMeasurement(): ?string
    {
        return $this->measurement;
    }

    public function setMeasurement(?string $measurement): self
    {
        $this->measurement = $measurement;

        return $this;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit(?string $unit): self
    {
        $this->unit = $unit;

        return $this;
    }
}
