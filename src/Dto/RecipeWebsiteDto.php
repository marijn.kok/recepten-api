<?php

declare(strict_types=1);

namespace App\Dto;

use RuntimeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class RecipeWebsiteDto
{
    public function __construct(
        #[Groups(['write'])]
        #[Assert\NotBlank(), Assert\Url]
        private readonly ?string $url = null
    ) {
    }

    public function getUrl(): string
    {
        if (!is_string($this->url)) {
            throw new RuntimeException('Should never be called before validation.');
        }

        return $this->url;
    }
}
