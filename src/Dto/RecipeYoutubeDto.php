<?php

declare(strict_types=1);

namespace App\Dto;

use RuntimeException;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @deprecated
 */
class RecipeYoutubeDto
{
    public function __construct(
        #[Groups(['write'])]
        #[Assert\NotBlank()]
        private readonly ?string $youtube = null
    ) {
    }

    public function getYoutube(): string
    {
        if (empty($this->youtube)) {
            throw new RuntimeException('Should never be called before validation.');
        }

        return $this->youtube;
    }
}
