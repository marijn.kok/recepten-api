<?php

declare(strict_types=1);

namespace App\Recipe;

use Exception;

class UnsupportedWebsiteException extends Exception
{
}
