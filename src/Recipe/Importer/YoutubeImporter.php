<?php

declare(strict_types=1);

namespace App\Recipe\Importer;

use App\Entity\Recipe;
use App\Recipe\ImporterInterface;
use App\Service\Google\GoogleClientException;
use App\Service\Google\InvalidVideoIdException;
use App\Service\Google\InvalidVideoUrlException;
use App\Service\Google\VideoResource;
use App\Service\Google\YoutubeService;
use App\Service\Google\YoutubeVideoNotFoundException;
use Google\Service\YouTube\Video;

class YoutubeImporter implements ImporterInterface
{
    public function __construct(
        private readonly YoutubeService $youtubeService,
    ) {
    }

    public function execute(string $url, Recipe $recipe): void
    {
        $video = $this->youtubeService->getVideo($url);
        if (!$video instanceof Video) {
            throw new YoutubeVideoNotFoundException('Could not find video in Youtube');
        }

        $recipe
            ->setYoutube($video->getId())
            ->setName($video->getSnippet()->getTitle())
            ->setDescription($video->getSnippet()->getDescription());
    }

    public function support(string $url): bool
    {
        try {
            $this->youtubeService->hasValidCredentials();
            (new VideoResource($url))->getVideoId();
        } catch (GoogleClientException|InvalidVideoIdException|InvalidVideoUrlException) {
            return false;
        }


        return true;
    }
}
