<?php

declare(strict_types=1);

namespace App\Recipe\Importer;

use App\Entity\Ingredient;
use App\Entity\Recipe;
use App\Entity\RecipeIngredient;
use App\Enum\Unit;
use App\Recipe\ImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class AlbertHeijnImporter implements ImporterInterface
{
    /**
     * @var array<string, Ingredient>
     */
    private array $ingredients = [];

    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function execute(string $url, Recipe $recipe): void
    {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $recipe
            ->setName($crawler->filterXPath('//*[contains(@class, "recipe-header_title")]')->first()->text())
            ->setDescription($crawler->filterXPath('//*[contains(@class, "recipe-header_subtitle")]')->first()->text());

        $this->extractIngredients($crawler, $recipe);
    }

    private function extractIngredients(Crawler $crawler, Recipe $recipe): void
    {
        $ingredientsCrawler = $crawler->filterXPath('//*[contains(@class, "recipe-ingredients_ingredientsList")]');
        $extractFunction = static fn(Crawler $extractCrawler) => $extractCrawler->text();
        $units = $ingredientsCrawler->filterXPath('//*[contains(@class, "ingredient_unit")]')->each($extractFunction);
        $ingredients =
            $ingredientsCrawler->filterXPath('//*[contains(@class, "ingredient_name")]')->each($extractFunction);

        if (count($ingredients) <= 0) {
            return;
        }

        foreach ($ingredients as $key => $ingredient) {
            $recipeIngredient = new RecipeIngredient();
            $unit = $units[$key] ?? null;

            $this->setMeasurement($unit, $recipeIngredient);
            $recipeIngredient
                ->setIngredient($this->getIngredient($ingredient));

            $recipe->addIngredient($recipeIngredient);
        }

        $this->entityManager->flush();
    }

    private function setMeasurement(?string $unit, RecipeIngredient $recipeIngredient): void
    {
        $exploded = explode(' ', trim($unit ?? ''));
        if (count($exploded) <= 1) {
            $recipeIngredient->setMeasurement($this->parseMeasurement($unit));

            return;
        }

        [$amount, $unitValue] = $exploded;
        $parsedUnit = $this->parseUnit(trim($unitValue));
        $parsedMeasurement = $this->parseMeasurement($amount);

        if ($parsedUnit === null) {
            $parsedMeasurement = $this->parseMeasurement($unit);
        }

        $recipeIngredient
            ->setUnit($parsedUnit)
            ->setMeasurement($parsedMeasurement);
    }

    private function parseMeasurement(mixed $amount = null): ?string
    {
        if (!is_string($amount)) {
            return null;
        }

        if (!str_contains($amount, '½')) {
            return $amount;
        }

        $amount = preg_replace('/(.*)(½)(.*)/u', '$1.5$3', $amount);
        if (!is_string($amount)) {
            return null;
        }

        if (str_starts_with($amount, '.5')) {
            $amount = "0{$amount}";
        }

        return $amount;
    }

    private function parseUnit(?string $unit = null): ?string
    {
        if (empty($unit)) {
            return null;
        }

        $unit = Unit::fromShortName(trim($unit));

        return $unit ? $unit->value : null;
    }

    private function getIngredient(string $ingredientValue): Ingredient
    {
        if (isset($this->ingredients[$ingredientValue])) {
            return $this->ingredients[$ingredientValue];
        }

        $ingredient = $this->entityManager->getRepository(Ingredient::class)->findOneBy(['name' => $ingredientValue]);
        if ($ingredient instanceof Ingredient) {
            return $ingredient;
        }

        $ingredient = (new Ingredient())->setName($ingredientValue);
        $this->entityManager->persist($ingredient);
        $this->ingredients[$ingredientValue] = $ingredient;

        return $ingredient;
    }

    public function support(string $url): bool
    {
        if (preg_match('/(.*)ah\.nl\/allerhande\/recept\/(.*)\/(.*)/i', $url)) {
            return true;
        }

        return false;
    }
}
