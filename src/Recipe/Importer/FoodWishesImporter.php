<?php

declare(strict_types=1);

namespace App\Recipe\Importer;

use App\Entity\Recipe;
use App\Recipe\ImporterInterface;
use App\Service\Google\InvalidVideoIdException;
use App\Service\Google\InvalidVideoUrlException;
use App\Service\Google\VideoResource;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class FoodWishesImporter implements ImporterInterface
{
    public function execute(string $url, Recipe $recipe): void
    {
        $client = new Client();
        $crawler = $client->request('GET', $url);

        $this->extractYoutubeVideo($crawler, $recipe);

        $recipe
            ->setName($crawler->filter('.post-title')->first()->text())
            ->setDescription($crawler->filter('.post-body')->first()->text());
    }

    private function extractYoutubeVideo(Crawler $crawler, Recipe $recipe): void
    {
        $youtubeEmbedUrl = $crawler->filterXPath('//*[contains(@src, "youtube.com")]')->first()->attr('src');
        if (empty($youtubeEmbedUrl)) {
            return;
        }

        $youtubeParts = explode('/', $youtubeEmbedUrl);
        if (!is_array($youtubeParts)) {
            return;
        }

        $videoId = end($youtubeParts);
        try {
            (new VideoResource($videoId))->getVideoId();
        } catch (InvalidVideoIdException|InvalidVideoUrlException) {
            return;
        }

        $recipe->setYoutube($videoId);
    }

    public function support(string $url): bool
    {
        if (preg_match('/(.*)foodwishes\.blogspot\.com\/(.*)\/(.*)/i', $url)) {
            return true;
        }

        return false;
    }
}
