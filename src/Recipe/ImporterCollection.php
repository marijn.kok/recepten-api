<?php

declare(strict_types=1);

namespace App\Recipe;

use Traversable;

class ImporterCollection
{
    /**
     * @param iterable<ImporterInterface> $importers
     */
    public function __construct(
        /** @var iterable<ImporterInterface> $importers */
        private readonly iterable $importers
    ) {
    }

    /**
     * @return Traversable<ImporterInterface>
     */
    public function getSupported(string $url): iterable
    {
        foreach ($this->importers as $importer) {
            if ($importer->support($url)) {
                yield $importer;
            }
        }
    }
}
