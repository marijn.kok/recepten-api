<?php

declare(strict_types=1);

namespace App\Recipe;

use App\Entity\Recipe;

interface ImporterInterface
{
    public function execute(string $url, Recipe $recipe): void;

    public function support(string $url): bool;
}
