<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;

abstract class AbstractDataTransformer implements DataTransformerInterface
{
    public function __construct(
        protected readonly ValidatorInterface $validator
    ) {
    }
}
