<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\RecipeWebsiteDto;
use App\Entity\Recipe;
use App\Recipe\ImporterCollection;
use App\Recipe\ImporterInterface;
use App\Recipe\UnsupportedWebsiteException;
use Generator;

class RecipeWebsiteDataTransformer extends AbstractDataTransformer
{
    public function __construct(
        private readonly ImporterCollection $importerCollection,
        ValidatorInterface $validator
    ) {
        parent::__construct($validator);
    }

    /**
     * @param RecipeWebsiteDto $dto
     */
    public function transform(
        $dto,
        string $to,
        array $context = []
    ): Recipe {
        $this->validator->validate($dto);

        $recipe = (new Recipe())->setSource($dto->getUrl());

        /** @var Generator<ImporterInterface> $importers */
        $importers = $this->importerCollection->getSupported($dto->getUrl());
        if (!$importers->valid()) {
            throw new UnsupportedWebsiteException('Website url is not supported');
        }

        foreach ($importers as $importer) {
            $importer->execute($dto->getUrl(), $recipe);
        }

        $this->validator->validate($recipe);

        return $recipe;
    }

    public function supportsTransformation(
        $data,
        string $to,
        array $context = []
    ): bool {
        if ($data instanceof Recipe) {
            return false;
        }

        return Recipe::class === $to && RecipeWebsiteDto::class === ($context['input']['class'] ?? null);
    }
}
