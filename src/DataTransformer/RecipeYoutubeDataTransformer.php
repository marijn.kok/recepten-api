<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Dto\RecipeYoutubeDto;
use App\Entity\Recipe;
use App\Service\Google\YoutubeService;
use App\Service\Google\YoutubeVideoNotFoundException;
use Google\Service\YouTube\Video;

/**
 * The importer method is a better method because it's more flexible
 *
 * @deprecated
 */
class RecipeYoutubeDataTransformer extends AbstractDataTransformer
{
    public function __construct(
        private readonly YoutubeService $youtubeService,
        ValidatorInterface $validator
    ) {
        parent::__construct($validator);
    }

    /**
     * @param RecipeYoutubeDto $dto
     */
    public function transform($dto, string $to, array $context = []): Recipe
    {
        $this->validator->validate($dto);

        $this->youtubeService->hasValidCredentials();

        $video = $this->youtubeService->getVideo($dto->getYoutube());
        if (!$video instanceof Video) {
            throw new YoutubeVideoNotFoundException('Could not find video in Youtube');
        }

        return (new Recipe())
            ->setYoutube($video->getId())
            ->setName($video->getSnippet()->getTitle())
            ->setDescription($video->getSnippet()->getDescription());
    }

    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof Recipe) {
            return false;
        }

        return Recipe::class === $to && RecipeYoutubeDto::class === ($context['input']['class'] ?? null);
    }
}
