<?php

declare(strict_types=1);

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use ApiPlatform\Core\DataPersister\ResumableDataPersisterInterface;
use App\Entity\AuthorInterface;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class AuthorPersister implements DataPersisterInterface, ResumableDataPersisterInterface
{
    public function __construct(private readonly Security $security)
    {
    }

    public function supports($data): bool
    {
        return $data instanceof AuthorInterface;
    }

    /**
     * @param AuthorInterface $data
     */
    public function persist($data): AuthorInterface
    {
        if ($data->getAuthor() instanceof User) {
            return $data;
        }

        $user = $this->security->getUser();
        if (!$user instanceof User) {
            return $data;
        }

        $data->setAuthor($user);

        return $data;
    }

    /**
     * @param AuthorInterface $data
     */
    public function remove($data): void
    {
    }

    /**
     * @param array<mixed> $context
     */
    public function resumable(array $context = []): bool
    {
        return true;
    }
}
