<?php

declare(strict_types=1);

namespace App\Enum;

/**
 * No cups or other imperial bs
 */
enum Unit: string
{
    # Volume units
    case LITER = 'Liter';
    case MILLILITER = 'Milliliter';
    case CENTILITER = 'Centiliter';
    case DECILITER = 'Deciliter';

    # Mass units
    case GRAM = 'Gram';
    case KILOGRAM = 'Kilogram';
    case POUND = 'Pound'; // ~0.45 kg
    case OUNCE = 'Ounce'; // ~28.35 g

    # Misc
    case AMOUNT = 'Amount';

    public static function fromShortName(string $shortName): ?Unit
    {
        return match ($shortName) {
            default => null,
            '' => self::AMOUNT,
            'l' => self::LITER,
            'ml' => self::MILLILITER,
            'cl' => self::CENTILITER,
            'dl' => self::DECILITER,
            'g' => self::GRAM,
            'kg' => self::KILOGRAM,
            'lb' => self::POUND,
            'oz' => self::OUNCE,
        };
    }

    public function shortName(): string
    {
        return match ($this) {
            self::LITER => 'l',
            self::MILLILITER => 'ml',
            self::CENTILITER => 'cl',
            self::DECILITER => 'dl',
            self::GRAM => 'g',
            self::KILOGRAM => 'kg',
            self::POUND => 'lb',
            self::OUNCE => 'oz',
            self::AMOUNT => '',
        };
    }
}
