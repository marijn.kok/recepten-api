# Recepten
Dit is een API waarbij je recepten kan aanmaken of kan opslaan vanuit externen bronnen waaronder: Youtube, Albert Heijn en FoodWishes 

Bij de externe bronnen worden de receptgegevens automatisch opgehaald en ingevuld zover dat mogelijk is.

### Proof of concept
Niet alles is volledig uitgewerkt, zo zijn er bereidingsstappen, moeilijkheid, dieet, bereidingsduur, etc niet toegevoegd.

### Toekomst
Misschien wil ik dit in de toekomst door ontwikkelen en er een app bij maken of koppelen aan iets als Homeassistant.

### Waarom een api en geen webview?
Omdat ik nog niet weet waaraan en hoe ik dit wil gaan koppelen. 

# Todo
 - [ ] Add security voters for the Recipe, RecipeIngredient and ingredient mutations
 - [ ] Add endpoints to manage your own user
 - [ ] Add password reset option (add user e-mail address)
 - [ ] Improve Recipe, RecipeIngredient and ingredient tests in combination with the authentication
 - [ ] Complete the Recipe fields (prep duration, stars, dieet, etc)
 - [ ] Add more importers for the more popular websites
 - [ ] Add e-number cheatsheet
 - [ ] Add recipe favorites
 - [ ] Add improved recipe filters (if needed and useful)

# Project opstarten / setup

### Requirements
Je hebt alleen docker nodig.

### Voer de volgende commands uit om het project op te starten.
```
docker-compose up -d
docker-compose exec php bash
cp .env.docker .env
composer install
php bin/console assets:install
composer dr
```
Nu kan je naar http://localhost:8080 gaan om de swagger/redoc documentatie te bekijken.

### Tests & Quality assurance
In het project zit phpstan en phpunit tests die je op de volgende manier kan draaien
```
# Voert phpunit & phpstan uit
composer qa 

php bin/phpunit
php vendor/bin/phpstan
```

### Composer scripts
Voor makkelijke development over meerdere projecten gebruik ik composer scripts, dit zijn scripts die je via composer kan defineren en voeren.
```
composer dr # refreshed de volledige database en voert de fixtures uit
composer mdiff # maakt een nieuwe migratie aan indien dit nodig is (php bin/console doctrine:migrations:diff)
composer qa # Voert phpunit en phpstan uit

# composer.json
{
    "scripts": {
         "qa": [
            "@phpstan",
            "@phpunit"
        ],
        "phpstan": "vendor/bin/phpstan analyse -c phpstan.neon",
        "phpunit": "php bin/phpunit",
        "dr": [
            "php bin/console cache:clear",
            "@dd",
            "@dm",
            "@df"
        ],
        "mdiff": "php bin/console doctrine:migrations:diff",
        "dd": [
            "php bin/console doctrine:database:drop --force --if-exists",
            "php bin/console doctrine:database:create --if-not-exists"
        ],
        "dm": "php bin/console doctrine:migrations:migrate -n",
        "df": "php bin/console hautelook:fixtures:load -n"
    }
}
```

# API 
De documentatie is te vinden op http://localhost:8080.

Je kan inloggen met de accounts: `user & admin`

Het wachtwoord is altijd `test`

### Inloggen (JWT token opvragen)
```
curl --location --request POST 'localhost:8080/authentication_token' \
--header 'Content-Type: application/ld+json; charset=utf-8' \
--data-raw '{
    "username": "user",
    "password": "test"
}'
```

### Makkelijk recepten aanmaken
Albert Heijn
```
curl --location --request POST 'localhost:8080/recipes/website' \
--header 'Content-Type: application/ld+json; charset=utf-8' \
--data-raw '{
    "url":"https://www.ah.nl/allerhande/recept/R-R1196155/oesterzwamkebab"
}'
```

Youtube filmpjes (Zie de youtube kop hieronder voor meer uitleg)
```
curl --location --request POST 'localhost:8080/recipes/website' \
--header 'Content-Type: application/ld+json; charset=utf-8' \
--data-raw '{
    "url":"https://www.youtube.com/watch?v=iRBaIEI0GDE"
}'
```

FoodWishes
```
curl --location --request POST 'localhost:8080/recipes/website' \
--header 'Content-Type: application/ld+json; charset=utf-8' \
--data-raw '{
    "url":"https://foodwishes.blogspot.com/2013/04/pita-bread-whats-in-your-pocket.html"
}'
```

# Youtube
Op Youtube staan veel leuke recepten het zou dus handig zijn door een recept te kunnen toevoegen door alleen de url of het id op te geven.

### Google api key
Voor deze functionaliteit heb je wel een api key nodig voor de youtube api. Je kan er makkelijk en gratis een aanmaken via de link hier onder.
https://developers.google.com/youtube/v3/getting-started

Als er geen geldige api key is toegevoegd dan krijg je een mooie http status 418 I'm a teapot error :-)
De API key kan je in de .env toevoegen aan `GOOGLE_API_KEY`

### Request
Je kan het youtube ID mee geven of de volledige url.
```
POST localhost:8080/recipes/youtube
BODY
{
  "youtube": "dQw4w9WgXcQ"
}
```
