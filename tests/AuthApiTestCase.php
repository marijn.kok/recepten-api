<?php

declare(strict_types=1);

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;

abstract class AuthApiTestCase extends ApiTestCase
{
    protected function authenticatedClient(string $username = 'user'): Client
    {
        $client = static::createClient();

        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => $username,
                'password' => 'test',
            ],
        ]);

        $json = $response->toArray();
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $json);

        $client->setDefaultOptions(['auth_bearer' => $json['token']]);
        return $client;
    }
}
