<?php

declare(strict_types=1);

namespace App\Tests\functional;

use App\Entity\Recipe;
use App\Entity\User;
use App\Tests\AuthApiTestCase;
use DateTime;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;
use Symfony\Contracts\HttpClient\ResponseInterface;

class RecipesTest extends AuthApiTestCase
{
    use RefreshDatabaseTrait;

    public function testGetRecipe(): void
    {
        $client = $this->authenticatedClient();
        $client->request('GET', '/recipes/1');
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceItemJsonSchema(Recipe::class);
    }

    public function testGetCollection(): void
    {
        $client = $this->authenticatedClient('user');
        $client->request('GET', '/recipes');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceCollectionJsonSchema(Recipe::class);
        $this->assertJsonContains(
            [
                '@context'         => '/contexts/Recipe',
                '@id'              => '/recipes',
                '@type'            => 'hydra:Collection',
                'hydra:totalItems' => 11,
                'hydra:member'     => [
                    [
                        '@id'   => '/recipes/1',
                        '@type' => 'Recipe',
                    ],
                ],
            ]
        );

        $container = self::getContainer();
        /** @var User $user */
        $user = $container->get('doctrine')->getRepository(User::class)->findOneBy(['username' => 'user']);
        $this->assertInstanceOf(User::class, $user);

        $recipes = count($container->get('doctrine')->getRepository(Recipe::class)->findBy(['author' => $user]));
        $this->assertLessThan(11, $recipes);

        $client->request('GET', '/recipes?author=' . $user->getId());
        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceCollectionJsonSchema(Recipe::class);
        $this->assertJsonContains(
            [
                '@context'         => '/contexts/Recipe',
                '@id'              => '/recipes',
                '@type'            => 'hydra:Collection',
                'hydra:totalItems' => $recipes,
            ]
        );
    }

    public function testCreateRecipe(): void
    {
        $client = $this->authenticatedClient();
        $response = $client->request('POST', '/recipes', [
            'json' => [
                'name' => 'Bananen Spinazie Smoothie',
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(
            [
                '@context' => '/contexts/Recipe',
                '@type'    => 'Recipe',
                'name'     => 'Bananen Spinazie Smoothie',
            ]
        );
        $this->assertMatchesRegularExpression('~^/recipes/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(Recipe::class);
    }

    public function testCreateInvalidRecipe(): void
    {
        $client = $this->authenticatedClient();
        $client->request('POST', '/recipes', ['json' => []]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertJsonContains(
            [
                '@context'          => '/contexts/ConstraintViolationList',
                '@type'             => 'ConstraintViolationList',
                'hydra:title'       => 'An error occurred',
                'hydra:description' => 'name: This value should not be blank.',
            ]
        );
    }

    public function testUpdateRecipe(): void
    {
        $client = $this->authenticatedClient();
        /** @var string $iri */
        $iri = $this->findIriBy(Recipe::class, ['name' => 'Chickpea Curry']);

        /** @var ResponseInterface $response */
        $response = $client->request(
            'PUT',
            $iri,
            [
                'json' => [
                    'name' => 'Curry (Chickpea)',
                ],
            ]
        );

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains(
            [
                '@id'  => $iri,
                'name' => 'Curry (Chickpea)',
            ]
        );

        /** @var array $responseBody */
        $responseBody = json_decode($response->getContent(), true); // @phpstan-ignore-line
        $this->assertIsArray($responseBody);
        $updatedAt = new DateTime($responseBody['updatedAt']);
        $now = new DateTime();
        $this->assertEquals($updatedAt->format('Y-m-d H'), $now->format('Y-m-d H'));
    }
}
