<?php

declare(strict_types=1);

namespace App\Tests\functional;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use App\Entity\User;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class AuthenticationTest extends ApiTestCase
{
    use ReloadDatabaseTrait;

    public function testLogin(): void
    {
        $client = self::createClient();
        $container = self::getContainer();

        $user = new User();
        $user
            ->setUsername('test')
            ->setRoles([User::ROLE_USER])
            ->setPassword(
                $container->get('security.user_password_hasher')->hashPassword($user, 'super-safe-password')
            );

        $manager = $container->get('doctrine')->getManager();
        $manager->persist($user);
        $manager->flush();

        // retrieve a token
        $response = $client->request('POST', '/authentication_token', [
            'headers' => ['Content-Type' => 'application/json'],
            'json'    => [
                'email'    => 'test',
                'password' => 'super-safe-password',
            ],
        ]);

        $json = $response->toArray();
        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $json);

        // test not authorized
        $client->request('GET', '/ingredients');
        $this->assertResponseStatusCodeSame(401);

        // test authorized
        $client->request('GET', '/ingredients', ['auth_bearer' => $json['token']]);
        $this->assertResponseIsSuccessful();
    }
}
