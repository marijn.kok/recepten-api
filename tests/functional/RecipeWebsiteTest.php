<?php

declare(strict_types=1);

namespace App\Tests\functional;

use App\Entity\Recipe;
use App\Tests\AuthApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class RecipeWebsiteTest extends AuthApiTestCase
{
    use RefreshDatabaseTrait;

    public function testCreateAlbertHeijnRecipe(): void
    {
        $client = $this->authenticatedClient('admin');
        $response = $client->request('POST', '/recipes/website', [
            'json' => [
                'url' => 'https://www.ah.nl/allerhande/recept/R-R1196155/oesterzwamkebab',
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(
            [
                '@context'    => '/contexts/Recipe',
                '@type'       => 'Recipe',
                'name'        => 'Oesterzwamkebab',
                'description' => 'De oesterzwam is een echte alleskunner. Met dit recept maak je er heerlijke vegan \'kebab\' van. Het proberen waard!',
            ]
        );
        $this->assertMatchesResourceItemJsonSchema(Recipe::class);
        $responseBody = $response->toArray();
        $this->assertMatchesRegularExpression('~^/recipes/\d+$~', $responseBody['@id']);
        $this->assertCount(16, $responseBody['ingredients']);
    }
}
