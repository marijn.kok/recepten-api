<?php

declare(strict_types=1);

namespace App\Tests\functional;

use App\Entity\RecipeIngredient;
use App\Enum\Unit;
use App\Tests\AuthApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class RecipeIngredientTest extends AuthApiTestCase
{
    use RefreshDatabaseTrait;

    public function testGetRecipeIngredient(): void
    {
        $client = $this->authenticatedClient();
        $client->request('GET', '/recipe_ingredients/1');
        $this->assertResponseStatusCodeSame(404);
    }

    public function testGetCollection(): void
    {
        $client = $this->authenticatedClient();
        $client->request('GET', '/recipes/1/ingredients');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertMatchesResourceCollectionJsonSchema(RecipeIngredient::class);
        $this->assertJsonContains(
            [
                '@context'         => '/contexts/RecipeIngredient',
                '@id'              => '/recipes/1/ingredients',
                '@type'            => 'hydra:Collection',
                'hydra:totalItems' => 5,
                'hydra:member'     => [
                    [
                        '@id'   => '/recipe_ingredients/1',
                        '@type' => 'RecipeIngredient',
                    ],
                ],
            ]
        );
    }

    public function testCreateRecipeIngredient(): void
    {
        $client = $this->authenticatedClient();

        $response = $client->request('POST', '/recipes/ingredient', [
            'json' => [
                'recipe'      => '/recipes/1',
                'ingredient'  => '/ingredients/1',
                'measurement' => '100',
                'unit'        => Unit::LITER->value,
            ],
        ]);

        $this->assertResponseStatusCodeSame(201);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(
            [
                '@context' => '/contexts/RecipeIngredient',
                '@type'    => 'RecipeIngredient',
                'unit'     => Unit::LITER->value,
            ]
        );
        $this->assertMatchesRegularExpression('~^/recipe_ingredients/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(RecipeIngredient::class);
    }

    public function testCreateInvalidRecipeIngredient(): void
    {
        $client = $this->authenticatedClient();

        $client->request('POST', '/recipes/ingredient', [
            'json' => [
                'recipe'      => '/recipes/1',
                'ingredient'  => '/ingredients/1',
                'measurement' => '100',
                'unit'        => 'Oeps',
            ],
        ]);

        $this->assertResponseStatusCodeSame(422);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        $this->assertJsonContains(
            [
                '@context'          => '/contexts/ConstraintViolationList',
                '@type'             => 'ConstraintViolationList',
                'hydra:title'       => 'An error occurred',
                'hydra:description' => 'unit: The enum case ""Oeps"" is not valid. Valid cases are: ""Liter", "Milliliter", "Centiliter", "Deciliter", "Gram", "Kilogram", "Pound", "Ounce", "Amount""',
            ]
        );
    }

    public function testUpdateRecipeIngredient(): void
    {
        $ingredient = $this->getContainer()->get('doctrine')->getRepository(RecipeIngredient::class)->find(1);
        $this->assertInstanceOf(RecipeIngredient::class, $ingredient);

        $ingredientId = $ingredient->getIngredient()->getId(); // @phpstan-ignore-line
        $newIngredientId = $ingredientId > 1 ? $ingredientId - 1 : $ingredientId + 1;

        $client = $this->authenticatedClient();

        $response = $client->request('PUT', '/recipes/ingredient/1', [
            'json' => [
                'ingredient'  => '/ingredients/' . $newIngredientId,
                'measurement' => '1',
                'unit'        => Unit::KILOGRAM->value,
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(
            [
                '@context'    => '/contexts/RecipeIngredient',
                '@type'       => 'RecipeIngredient',
                'ingredient'  => '/ingredients/' . $newIngredientId,
                'measurement' => '1',
                'unit'        => Unit::KILOGRAM->value,
            ]
        );
        $this->assertMatchesRegularExpression('~^/recipe_ingredients/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(RecipeIngredient::class);
    }

    public function testUpdateCannotChangeRecipe(): void
    {
        $ingredient = $this->getContainer()->get('doctrine')->getRepository(RecipeIngredient::class)->find(1);
        $this->assertInstanceOf(RecipeIngredient::class, $ingredient);
        $this->assertSame($ingredient->getRecipe()->getId(), 1); // @phpstan-ignore-line
        $ingredientId = $ingredient->getIngredient()->getId();  // @phpstan-ignore-line
        $newIngredientId = $ingredientId > 1 ? $ingredientId - 1 : $ingredientId + 1;

        $client = $this->authenticatedClient();
        $response = $client->request('PUT', '/recipes/ingredient/1', [
            'json' => [
                'recipe'      => '/recipes/2',
                'ingredient'  => '/ingredients/' . $newIngredientId,
                'measurement' => '1',
                'unit'        => Unit::KILOGRAM->value,
            ],
        ]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');
        $this->assertJsonContains(
            [
                '@context'    => '/contexts/RecipeIngredient',
                '@type'       => 'RecipeIngredient',
                'ingredient'  => '/ingredients/' . $newIngredientId,
                'measurement' => '1',
                'unit'        => Unit::KILOGRAM->value,
            ]
        );
        $this->assertMatchesRegularExpression('~^/recipe_ingredients/\d+$~', $response->toArray()['@id']);
        $this->assertMatchesResourceItemJsonSchema(RecipeIngredient::class);

        $ingredient = $this->getContainer()->get('doctrine')->getRepository(RecipeIngredient::class)->find(1);
        $this->assertInstanceOf(RecipeIngredient::class, $ingredient);
        $this->assertSame($ingredient->getRecipe()->getId(), 1);  // @phpstan-ignore-line
        $this->assertNotSame($ingredient->getIngredient()->getId(), $ingredientId);  // @phpstan-ignore-line
    }

    public function testDeleteRecipeIngredient(): void
    {
        $this->assertNotNull(
            static::getContainer()->get('doctrine')->getRepository(RecipeIngredient::class)->find(1)
        );

        $client = $this->authenticatedClient();
        $client->request('DELETE', '/recipes/ingredient/1');
        $this->assertResponseStatusCodeSame(204);

        $this->assertNull(
            static::getContainer()->get('doctrine')->getRepository(RecipeIngredient::class)->find(1)
        );
    }
}
