<?php

declare(strict_types=1);

namespace App\Tests\unit;

use App\Recipe\Importer\AlbertHeijnImporter;
use App\Recipe\Importer\FoodWishesImporter;
use App\Recipe\Importer\YoutubeImporter;
use App\Recipe\ImporterCollection;
use App\Recipe\ImporterInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RecipeImporterTest extends KernelTestCase
{
    /**
     * @param array<class-string> $expected
     *
     * @dataProvider websiteProvider
     */
    public function testImporterSupport(array $expected, string $input): void
    {
        $importerCollection = $this->getContainer()->get(ImporterCollection::class);

        $importers = $importerCollection->getSupported($input);
        $importers = array_map(
            static fn(ImporterInterface $importer) => $importer::class,
            iterator_to_array($importers)
        );
        $this->assertSame($expected, $importers);
    }

    /**
     * @return array{array{array{string}, string}}
     */
    public function websiteProvider(): array
    {
        return [
            [[AlbertHeijnImporter::class], 'https://www.ah.nl/allerhande/recept/R-R1196155/oesterzwamkebab'],
            [[YoutubeImporter::class], 'https://www.youtube.com/watch?v=NPiA69p4gqE'],
            [
                [FoodWishesImporter::class],
                'https://foodwishes.blogspot.com/2013/04/pita-bread-whats-in-your-pocket.html',
            ],
        ];
    }
}
