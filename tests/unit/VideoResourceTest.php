<?php

declare(strict_types=1);

namespace App\Tests\unit;

use App\Service\Google\InvalidVideoIdException;
use App\Service\Google\InvalidVideoUrlException;
use App\Service\Google\VideoResource;
use PHPUnit\Framework\TestCase;
use Throwable;

class VideoResourceTest extends TestCase
{
    /**
     * @dataProvider youtubeIdsProvider
     *
     *  @param class-string<Throwable>|null $exceptionClass
     */
    public function testVideoResources(string $resource, ?string $exceptionClass = null): void
    {
        $videoResource = new VideoResource($resource);
        if (is_string($exceptionClass) && class_exists($exceptionClass)) {
            $this->expectException($exceptionClass);
            $videoResource->getVideoId();

            return;
        }

        $this->assertIsString($videoResource->getVideoId());
    }

    /**
     * @return  array<string, array<int, string|null>>
     */
    public function youtubeIdsProvider(): array
    {
        return [
            'Valid youtube url' => ['https://www.youtube.com/watch?v=dQw4w9WgXcQ', null],
            'Valid youtube id' => ['dQw4w9WgXcQ', null],
            'Youtube id is too short' => ['dQw4w9WgXc', InvalidVideoIdException::class],
            'Youtube id is too long' => ['dQw4w9WgXcQc', InvalidVideoIdException::class],
            'Valid youtube url, id is too short' => ['https://www.youtube.com/watch?v=dQw4w9WgXc', InvalidVideoUrlException::class],
            'Invalid youtube url' => ['https://www.youtube.com/watch?dQw4w9WgXc', InvalidVideoUrlException::class],
            'Invalid youtube url (vimeo)' => ['https://www.vimeo.com/watch?v=dQw4w9WgXcQ', InvalidVideoUrlException::class],
        ];
    }
}
